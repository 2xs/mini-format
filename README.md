mini-format is a minimalist formating crate.

Its main purpose is to be used in very small embedded systems in which
you want to avoid use the [`core::fmt::Write`] when you need only
simply formatted output for debuging purpose.

Only decimal and hexadecimal formatting is provided and only for types
that can implement [`Into<u64>`] + [`Clone`] for implementation
simplicity.

[`Into<u64>`]: https://doc.rust-lang.org/core/convert/trait.Into.html
[`Clone`]: https://doc.rust-lang.org/core/clone/trait.Clone.html

The crate is `[no_std]` so it can be easily embedded in a baremetal
code.

# Examples

```
# use mini_format::format_hex;
let x: u64 = 0xFEDECAFADEBEEF;
let mut s = String::new();
format_hex(x, |c| s.push(c));
assert_eq!(s, String::from("00FEDECAFADEBEEF"));
```
