#![doc = include_str!("../README.md")]
#![cfg_attr(not(test), no_std)]

/// A trait that tells that a type can be formatted.
///
/// This is used to provide the [`format_dec`] and [`format_hex`]
/// function that can be called on any type that implement the trait
///
/// An implementation is provided for all types that implement
/// [`Into<u64>`]  and  [`Clone`]. In this default implementation, the
/// [`format_hex`] function always display leading zeros and always
/// shows the value as a 64 bits value
pub trait Format<F>
where
    F: FnMut(char),
{
    /// format to a decimal representation by using the provided
    /// `putchar` closure to *output* subsequent char, whatever it may
    /// mean
    fn format_dec(&self, putchar: F);
    /// format to a decimal representation by using the provided
    /// `putchar` closure to *output* subsequent char, whatever it may
    /// mean.
    fn format_hex(&self, putchar: F);

}

/// format a value to a decimal representation by using the provided
/// `putchar` closure to *output* subsequent char, whatever it may
/// mean
pub fn format_dec<T, F>(value: T, putchar: F)
where
    T: Format<F>,
    F: FnMut(char),
{
    value.format_dec(putchar);
}

/// format a value to a decimal representation by using the provided
/// `putchar` closure to *output* subsequent char, whatever it may
/// mean
pub fn format_hex<T, F>(value: T, putchar: F)
where
    T: Format<F>,
    F: FnMut(char),
{
    value.format_hex(putchar);
}


const TO_CHAR: &[u8] = b"0123456789ABCDEF";

/// Blanket implementation for all type that can be converted into
/// u64
impl<T,F> Format<F> for T
where
    T: Into<u64> + Clone,
    F: FnMut(char),
{
    fn format_dec(&self, mut putchar: F) {
        let mut max_pow = 1;
        let mut val: u64 = self.clone().into();

        while val / max_pow >= 10 {
            max_pow *= 10;
        }
        while max_pow >= 1 {
            let char_val = TO_CHAR[(val / max_pow) as usize] as char;
            putchar(char_val);
            val = val % max_pow;
            max_pow /= 10
        }
    }

    /// Format to an hexadecimal representation. It will always
    /// display leading 0 and always display a 64bits output whatever
    /// the original type is.
    fn format_hex(&self, mut putchar: F) {
        let val: u64 = self.clone().into();
        putchar(TO_CHAR[((val >> 60) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 56) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 52) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 48) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 44) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 40) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 36) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 32) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 28) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 24) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 20) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 16) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 12) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 8) & 0xf) as usize] as char);
        putchar(TO_CHAR[((val >> 4) & 0xf) as usize] as char);
        putchar(TO_CHAR[(val& 0xf) as usize] as char);
    }
    
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format_dec_u8() {
        let x: u8 = 13;
        let mut s = String::new();
        format_dec(x, |c| s.push(c));
        assert_eq!(s, format!("{x}"));
    }
    #[test]
    fn format_dec_u16() {
        let x: u16 = 854;
        let mut s = String::new();
        format_dec(x, |c| s.push(c));
        assert_eq!(s, format!("{x}"));
    }
    #[test]
    fn format_dec_u32() {
        let x: u32 = 123854;
        let mut s = String::new();
        format_dec(x, |c| s.push(c));
        assert_eq!(s, format!("{x}"));
    }
    #[test]
    fn format_dec_u64() {
        let x: u64 = 12345678901;
        let mut s = String::new();
        format_dec(x, |c| s.push(c));
        assert_eq!(s, format!("{x}"));
    }
    #[test]
    fn format_hex_u64() {
        let x: u64 = 0xCAFEDECAFADEBEEF;
        let mut s = String::new();
        format_hex(x, |c| s.push(c));
        assert_eq!(s, String::from("CAFEDECAFADEBEEF"));
    }
}
